package com.github.chengcheng.registration.registrationuser.entity

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.time.LocalDateTime

@Table("registration_pay")
data class RegistrationPay(
        @Id
        var id:Long?=null,
        @Column("info_id")
        var infoId:Long?=null,
        @Column("user_id")
        var userId:Long?=null,
        @Column("moeny")
        var money: BigDecimal?=null,
        @Column("info_name")
        var infoName:String?=null,
        @Column("type")
        var type:String?=null,
        @Column("registration_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var registrationTime:LocalDateTime?=null,
        @Column("examination_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var examinationTime:LocalDateTime?=null,
        @Column("publication_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var publicationTime:LocalDateTime?=null,
        @Column("transaction_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var transactiontime:LocalDateTime?=null
)
package com.github.chengcheng.registration.registrationuser.entity

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDateTime

@Table("registration_reservation")
open class RegistrationReservation{
        @Id
        var Id:Long?=null

        @Column("info_id")
        var infoId:Long?=null

        @Column("user_id")
        var userId:Long?=null

        @Column("registration_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var registrationtime:LocalDateTime?=null
}
package com.github.chengcheng.registration.registrationuser.repository

import com.github.chengcheng.registration.registrationuser.entity.RegistrationRole
import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface RegistrationRoleRepository: CoroutineCrudRepository<RegistrationRole, Long> {

    @Query("select * from registration_role where role_value=:roleValue")
    suspend fun findRoleByRoleValue(roleValue:String?):Flow<RegistrationRole>

    @Query("SELECT ro.* FROM `registration_user_role` as ur \n" +
            "RIGHT JOIN `registration_role` as ro ON ro.id=ur.role_id \n" +
            "WHERE ur.user_id=:userId")
    fun findUserCorrespondRoleList(userId:Long): Flow<RegistrationRole>
}
package com.github.chengcheng.registration.registrationuser.repository

import com.github.chengcheng.registration.registrationuser.entity.RegistrationUserRoleRelation
import kotlinx.coroutines.flow.Flow

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface RegistrationUserRoleRelationRepository : CoroutineCrudRepository<RegistrationUserRoleRelation, Long> {

    @Query("select * from registration_user_role where user_id=:userId and role_id=:roleId")
    fun checkUserRoleRelation(userId:Long,roleId:Long): Flow<RegistrationUserRoleRelation>

}
package com.github.chengcheng.registration.registrationuser.config

import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableTransactionManagement
@EnableR2dbcRepositories(basePackages = ["com.github.chengcheng.registration.registrationuser.repository"])
class R2DBCMysqlConfiguration
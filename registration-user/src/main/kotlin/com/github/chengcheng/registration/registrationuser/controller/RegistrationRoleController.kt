package com.github.chengcheng.registration.registrationuser.controller

import com.github.chengcheng.registration.registrationcommon.ResponseInfo
import com.github.chengcheng.registration.registrationuser.entity.RegistrationRole
import com.github.chengcheng.registration.registrationuser.entity.RegistrationUserRoleRelation
import com.github.chengcheng.registration.registrationuser.service.RegistrationRoleService
import com.github.chengcheng.registration.registrationuser.service.RegistrationUserRoleRelationService
import kotlinx.coroutines.reactor.mono
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RequestMapping("/role")
@RestController
class RegistrationRoleController {
    @Autowired
    lateinit var registrationRoleService: RegistrationRoleService
    @Autowired
    lateinit var registrationUserRoleRelationService: RegistrationUserRoleRelationService

    /**
     * @api{POST} /role 添加角色
     * @apiGroup 角色接口
     * @apiName 添加角色
     * @apiVersion 0.0.1
     * @apiDescription 用于添加角色
     *
     * @apiParam {String} roleName 角色名
     * @apiParam {String} roleValue 角色值
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "添加成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiSampleRequest /role
     */
    @PreAuthorize("hasRole('admin')")
    @PostMapping
    fun addRole(@RequestBody role: RegistrationRole):Mono<ResponseInfo<String>>{
        return mono{
           val flag = registrationRoleService.addRole(role)
            when(flag){
                0-> return@mono ResponseInfo.sucess("添加成功")
                else  ->  ResponseInfo.failed("添加失败,或当前角色所对应的值已存在")
            }
        }
    }


    /**
     * @api{PUT} /role 更新角色信息
     * @apiGroup 角色接口
     * @apiName 更新角色信息
     * @apiVersion 0.0.1
     * @apiDescription  用于更新角色信息
     *
     * @apiParam {Number} Id 角色id
     * @apiParam {String} roleName 角色名
     * @apiParam {String} roleValue 角色值
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "修改成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":"修改失败,或角色id不能为空"}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /role
     */
    @PreAuthorize("hasRole('admin')")
    @PutMapping
    fun updataRole(@RequestBody role:RegistrationRole):Mono<ResponseInfo<String>>{
        return  mono{
           val flag = registrationRoleService.updateItem(role)
            when(flag){
                0->return@mono ResponseInfo.sucess("修改成功")
                1->return@mono ResponseInfo.failed("修改失败,或角色id不能为空")
                2->return@mono ResponseInfo.failed("查询不到该角色")
                else->return@mono ResponseInfo.failed("更新失败")
            }
        }
    }


    /**
     * @api{GET} /role/all 获取全部角色
     * @apiGroup 角色接口
     * @apiName 获取全部角色
     * @apiVersion 0.0.1
     * @apiDescription 用于获取全部角色
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": [
     *    {
            "roleName": "",
            "roleValue": "",
            "creationTime": "",
            "id": ""
            }
        ]
    }
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /role/all
     */
    @PreAuthorize("hasRole('admin')")
    @GetMapping("/all")
    fun getAllRole():Mono<ResponseInfo<List<RegistrationRole>>>{
        return mono {
           return@mono ResponseInfo.sucess(registrationRoleService.getRoleList())
        }
    }

    /**
     * @api{Delete} /role/{id} 删除角色
     * @apiGroup 角色接口
     * @apiName 删除角色
     * @apiVersion 0.0.1
     * @apiDescription 用于删除角色
     *
     * @apiParam {Number} id 角色id
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "删除成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":"删除失败,不存在当前用户"}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /role/{id}
     */
    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("/{id}")
    fun deleteRoleById(@PathVariable("id") id:Long):Mono<ResponseInfo<String>>{
        return mono {
            val flag=registrationRoleService.deleteRole(id)
            if(flag==1){
                return@mono ResponseInfo.failed("删除失败,不存在当前用户")
            }
            return@mono ResponseInfo.sucess("删除成功")
        }
    }

    /**
     * @api{POST} /role/relation 添加用户角色关系
     * @apiGroup 角色接口
     * @apiName 添加用户角色关系
     * @apiVersion 0.0.1
     * @apiDescription 用于添加用户角色关系
     *
     * @apiParam {Number} userId 用户id
     * @apiParam {Number} roleId 角色id
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "保存成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":"保存失败,或当前已存在相同的关系"}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /role/relation
     */
    @PreAuthorize("hasRole('admin')")
    @PostMapping("/relation")
    fun addUserRoleRelation(@RequestBody registrationUserRoleRelation: RegistrationUserRoleRelation):Mono<ResponseInfo<String>>{
        print(registrationUserRoleRelation)
        return mono {
            val flag=registrationUserRoleRelationService.saveUserRoleRelation(registrationUserRoleRelation)
            when (flag){
                1-> return@mono ResponseInfo.failed("保存失败,或当前已存在相同的关系")
                2->return@mono ResponseInfo.failed("保存失败,当前用户不存在")
                3->return@mono ResponseInfo.failed("保存失败,当前角色不存在")
                0->return@mono  ResponseInfo.sucess("保存成功")
                else->return@mono ResponseInfo.failed("保存失败,位置错误")
            }
        }
    }

    /**
     * @api{POST} /role/relation/{id}  当前用户角色对应关系
     * @apiGroup 角色接口
     * @apiName 获取当前用户对应的角色
     * @apiVersion 0.0.1
     * @apiDescription 用于获取当前用户对应的角色
     *
     * @apiParam {Number} id 角色id
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": [
                {
                "userId": "",
                "roleId": "",
                "roleName": "",
                "roleValue": "",
                "createtime": "",
                "id": ""
                }
        ]
    }
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /role/relation/{id}
     */
    @PreAuthorize("hasRole('admin')")
    @GetMapping("/relation/{id}")
    fun getUserRoleRelation(@PathVariable("id")userId:Long):Mono<ResponseInfo<List<RegistrationRole>>>{
        return mono {
            return@mono ResponseInfo.sucess(registrationUserRoleRelationService.getUserRoleRelation(userId))
        }
    }
}
package com.github.chengcheng.registration.registrationuser.entity

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDateTime

@Table("registration_score")
data class RegistrationScore(
        @Id
        var id: Long?=null,
        @Column("info_id")
        var InfoId:Long?=null,
        @Column("user_id")
        var UserId:Long?=null,
        @Column("score")
        var score:Float?=null,
        @Column("create_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var createTime:LocalDateTime?=null
)
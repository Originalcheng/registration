package com.github.chengcheng.registration.registrationuser.repository

import com.github.chengcheng.registration.registrationuser.entity.RegistrationPay
import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface RegistrationPayRepository: CoroutineCrudRepository<RegistrationPay, Long> {

    @Query("select *from registration_pay where user_id=:userId and info_id=:infoId")
    fun findUserAndInfoPayment(userId:Long,infoId:Long): Flow<RegistrationPay>

    @Query("select *from registration_pay where user_id=:userId")
    fun findRegistrationPayByUserId(userId: Long):Flow<RegistrationPay>

    @Query("select *from registration_pay where info_id=:infoId")
    fun findRegistrationPayByInfoId(infoId: Long):Flow<RegistrationPay>
}
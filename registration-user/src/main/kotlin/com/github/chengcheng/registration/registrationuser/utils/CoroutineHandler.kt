package com.github.chengcheng.registration.registrationuser.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.reactor.mono
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.r2dbc.connectionfactory.R2dbcTransactionManager
import org.springframework.stereotype.Component
import org.springframework.transaction.reactive.TransactionalOperator
import reactor.core.publisher.Mono

@Component
class CoroutineHandler{
    @Autowired
    lateinit var transactionManager: R2dbcTransactionManager

    fun <T> suspendHandler(block:suspend CoroutineScope.()->T?):Mono<T>{
        val operator = TransactionalOperator.create(transactionManager)
        val result = mono(block = block)
        return operator.transactional(result)
    }
}
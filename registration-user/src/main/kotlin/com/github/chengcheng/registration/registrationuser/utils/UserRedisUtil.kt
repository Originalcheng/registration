package com.github.chengcheng.registration.registrationuser.utils

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONObject
import com.github.chengcheng.registration.registrationuser.entity.RegistrationRole
import com.github.chengcheng.registration.registrationuser.entity.bo.UserEntityDetails
import com.github.chengcheng.registration.registrationuser.entity.dto.UserRoleRedisInfo
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit
import javax.annotation.Resource

@Component
class UserRedisUtil {

    @Resource
    private lateinit var redisTemplate: RedisTemplate<String, List<RegistrationRole>>

    fun saveUserEntityDetails(username: String,roles:List<RegistrationRole>){
        redisTemplate.opsForValue().set(username,roles)
    }

    fun getUserEntityDetails(username:String):List<RegistrationRole>?{
        return redisTemplate.opsForValue().get(username)
    }

}
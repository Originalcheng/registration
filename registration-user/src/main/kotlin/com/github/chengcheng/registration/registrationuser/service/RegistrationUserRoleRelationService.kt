package com.github.chengcheng.registration.registrationuser.service

import com.github.chengcheng.registration.registrationuser.entity.RegistrationUserRoleRelation
import com.github.chengcheng.registration.registrationuser.repository.RegistrationRoleRepository
import com.github.chengcheng.registration.registrationuser.repository.RegistrationUserRoleRelationRepository
import com.github.chengcheng.registration.registrationuser.repository.UserRepository
import kotlinx.coroutines.flow.toList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RegistrationUserRoleRelationService {

    @Autowired
    private lateinit var registrationUserRoleRelationRepository: RegistrationUserRoleRelationRepository
    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var registrationRoleRepository: RegistrationRoleRepository

    suspend fun checkUserRoleRelation(userId:Long,roleId:Long)=registrationUserRoleRelationRepository.checkUserRoleRelation(userId,roleId).toList()

    suspend fun saveUserRoleRelation(userRoleRelation: RegistrationUserRoleRelation):Int{
        val user=userRepository.findById(userRoleRelation.userId!!)
        val role=registrationRoleRepository.findById(userRoleRelation.roleId!!)
        if (!checkUserRoleRelation(userRoleRelation.userId!!,userRoleRelation.roleId!!).isEmpty())return 1
        if (user==null) return 2
        if (role==null) return 3
        registrationUserRoleRelationRepository.save(userRoleRelation)
        return 0
    }


    suspend fun getUserRoleRelation(userId:Long) = registrationRoleRepository.findUserCorrespondRoleList(userId).toList()
}
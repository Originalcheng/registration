package com.github.chengcheng.registration.registrationuser.repository

import com.github.chengcheng.registration.registrationuser.entity.UserEntity
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface UserRepository: CoroutineCrudRepository<UserEntity, Long> {

    @Query("select * from registration_user where user_name=:username limit 1")
    suspend fun findUserByUsername(username:String?):UserEntity?

}
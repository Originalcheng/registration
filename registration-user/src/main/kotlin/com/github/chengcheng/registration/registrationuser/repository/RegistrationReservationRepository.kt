package com.github.chengcheng.registration.registrationuser.repository

import com.github.chengcheng.registration.registrationuser.entity.RegistrationReservation
import com.github.chengcheng.registration.registrationuser.entity.dto.ReservationInfo
import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface RegistrationReservationRepository: CoroutineCrudRepository<RegistrationReservation, Long> {

    @Query("select * from registration_reservation where user_id=:userId and info_id=:infoId")
    fun checkCurrentUserIsReserve(userId: Long,infoId: Long):Flow<RegistrationReservation>

    @Query(value = "select rs.id,rs.info_id,rs.user_id,ri.`name` as `subjects`,ri.type ,ri.money,ri.registration_time,ri.examination_time,ri.publication_time,rs.registration_time as `reservation_time` from registration_reservation as rs,registration_info as ri where user_id=:userId and ri.id=rs.info_id")
    fun getCurrentUserReserveInfo(@Param("userId") userId: Long):Flow<ReservationInfo>

    @Query(value = "select rs.id,rs.info_id,rs.user_id,ri.`name` as `subjects`,ri.type ,ri.money,ri.registration_time,ri.examination_time,ri.publication_time,rs.registration_time as `reservation_time` from registration_reservation as rs,registration_info as ri where  ri.id=rs.info_id")
    fun getAllReserveInfo():Flow<ReservationInfo>

    @Query(value = "DELETE FROM registration_reservation WHERE registration_reservation.info_id=:infoId")
    fun deleteByInfoId(infoId: Long)
}
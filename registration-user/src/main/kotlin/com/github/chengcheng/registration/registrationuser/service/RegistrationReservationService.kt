package com.github.chengcheng.registration.registrationuser.service

import com.github.chengcheng.registration.registrationuser.entity.RegistrationReservation
import com.github.chengcheng.registration.registrationuser.repository.RegistrationInfoRepository
import com.github.chengcheng.registration.registrationuser.repository.RegistrationReservationRepository
import kotlinx.coroutines.flow.toList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.ZoneOffset

@Service
class RegistrationReservationService {
    @Autowired
    private lateinit var registrationReservationRepository: RegistrationReservationRepository
    @Autowired
    private lateinit var registrationInfoRepository: RegistrationInfoRepository

    suspend fun reserve(registrationReservation: RegistrationReservation):Int{
        val info=registrationInfoRepository.findById(registrationReservation.infoId!!)
        val reserveInfo = registrationReservationRepository
                .checkCurrentUserIsReserve(registrationReservation.userId!!,registrationReservation.infoId!!)
                .toList()
        if (info!=null&&reserveInfo.isEmpty()){
            if(info.registrationtime!!.toInstant(ZoneOffset.of("+8")).toEpochMilli()<
            LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli()
            && info.examinationtime!!.toInstant(ZoneOffset.of("+8")).toEpochMilli()>
            LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli()) {
                registrationReservation.registrationtime = LocalDateTime.now()
                registrationReservationRepository.save(registrationReservation)
                return 0
            }else{
                return 2
            }
        }
       return 1
    }

    suspend fun cancelReserve(id:Long):Int{
        if(registrationReservationRepository.findById(id)!=null){
            registrationReservationRepository.deleteById(id)
            return 1
        }
        return 0
    }

    suspend fun getAllReserveInfo()=registrationReservationRepository.getAllReserveInfo().toList()

    suspend fun getCurrentUserAllReserveInfo(userId:Long)=registrationReservationRepository.getCurrentUserReserveInfo(userId).toList()


}
package com.github.chengcheng.registration.registrationuser

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RegistrationUserApplication

fun main(args: Array<String>) {
    runApplication<RegistrationUserApplication>(*args)
}

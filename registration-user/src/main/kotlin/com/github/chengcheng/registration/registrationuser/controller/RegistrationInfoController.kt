package com.github.chengcheng.registration.registrationuser.controller

import com.github.chengcheng.registration.registrationcommon.ResponseInfo
import com.github.chengcheng.registration.registrationuser.entity.RegistrationInfo
import com.github.chengcheng.registration.registrationuser.service.RegistrationInfoService
import kotlinx.coroutines.reactor.mono
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/exam")
class RegistrationInfoController {

    @Autowired
    lateinit var registrationInfoService: RegistrationInfoService



    /**
     * @api{POST} /exam 保存考试信息
     * @apiGroup 考试接口
     * @apiName 保存考试信息
     * @apiVersion 0.0.1
     * @apiDescription 保存考试信息
     *
     * @apiParam {String} subjects  科目
     * @apiParam {String} type  类型
     * @apiParam {String} money  报名费用
     * @apiParam {Date} registrationtime  报名时间
     * @apiParam {Date} examinationtime  考试时间
     * @apiParam {Date} publicationtime  公布时间
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "保存成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /exam
     */
    @PreAuthorize("hasRole('admin')")
    @PostMapping
    fun saveRegistrationInfo(@RequestBody registrationInfo: RegistrationInfo):Mono<ResponseInfo<String>>{
        println(registrationInfo)
        return registrationInfoService.saveInfo(registrationInfo)
            .flatMap<ResponseInfo<String>> { Mono.just(ResponseInfo.sucess("保存成功")) }
            .switchIfEmpty(Mono.just(ResponseInfo.failed("保存失败")))
    }


    /**
     * @api{GET} /exam/all  获取所有考试信息
     * @apiGroup 考试接口
     * @apiName 获取全部考试信息
     * @apiVersion 0.0.1
     * @apiDescription  获取全部考试信息
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": [
     *    {
            "subjects": "语文",
            "type": "统考",
            "moeny": 40.00,
            "registrationtime": "2020-07-06 12:42:34",
            "examinationtime": "2020-07-15 12:42:34",
            "publicationtime": "2020-07-31 21:46:33",
            "id": 3
        },
        {
        "subjects": "数学",
        "type": "统考",
        "moeny": 80.00,
        "registrationtime": "2020-07-06 12:42:34",
        "examinationtime": "2020-07-24 12:42:34",
        "publicationtime": "2020-07-30 21:46:33",
        "id": 4
        }
     * ]}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /exam/all
     */
    @GetMapping("/all")
    fun getALLRegistrationInfo():Mono<ResponseInfo<List<RegistrationInfo>>>{
        return mono{
           ResponseInfo.sucess(registrationInfoService.getAll())
        }
    }


    /**
     * @api{GET} /exam/{id} 获取考试信息
     * @apiGroup 考试接口
     * @apiName 获取考试信息
     * @apiVersion 0.0.1
     * @apiDescription 用于获取考试信息
     *
     * @apiParam {number} id 考试信息id
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": {
    *  "subjects": "数学",
    *  "type": "统考",
    *  "money": 80.00,
    *  "registrationtime": "2020-07-06 12:42:34",
    *  "examinationtime": "2020-07-24 12:42:34",
    *  "publicationtime": "2020-07-30 21:46:33",
    *  "id": 5
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /exam/{id}
     */
    @GetMapping("/{id}")
    fun getExamByID(@PathVariable id:Long):Mono<ResponseInfo<out Any>>{
        return mono {
            val exam=registrationInfoService.getInfoByID(id)
            if (exam==null){
                return@mono ResponseInfo.failed("查询不到当前考试信息")
            }
            return@mono ResponseInfo.sucess(exam)
        }
    }

    /**
     * @api{Delete} /exam//{id}  删除当前考试信息
     * @apiGroup 考试接口
     * @apiName  删除当前考试信息
     * @apiVersion 0.0.1
     * @apiDescription 删除当前考试信息
     *
     * @apiParam {Number} exam_id
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功",  "data": "删除成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":"删除失败,或当前考试不存在"}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /exam/{id}
     */
    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("/{id}")
    fun deleteRegistration(@PathVariable id:Long):Mono<ResponseInfo<String>>{
        return mono {
            val flag=registrationInfoService.deleteItemById(id)
            when(flag){
                1-> return@mono ResponseInfo.sucess("删除成功")
                else-> return@mono  ResponseInfo.sucess("删除失败,或当前考试不存在")
            }

        }
    }


    /**
     * @api{PUT} /exam 更新考试信息
     * @apiGroup 考试接口
     * @apiName 更新考试信息
     * @apiVersion 0.0.1
     * @apiDescription 用于更新考试信息
     *
     * @apiParam {String} id  科目
     * @apiParam {String} subjects  科目
     * @apiParam {String} type  类型
     * @apiParam {String} money  报名费用
     * @apiParam {Date} registrationtime  报名时间
     * @apiParam {Date} examinationtime  考试时间
     * @apiParam {Date} publicationtime  公布时间
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "保存成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /exam
     */
    @PutMapping
    fun updateExamInfo(@RequestBody registrationInfo: RegistrationInfo):Mono<ResponseInfo<String>>{
        return mono {
            val flag=registrationInfoService.updateInfo(registrationInfo)
            when(flag){
                0->return@mono ResponseInfo.sucess("更新成功")
                1->return@mono ResponseInfo.failed("更新信息不全")
                2->return@mono ResponseInfo.failed("查不到该考试相关信息")
                else->return@mono ResponseInfo.failed("更新失败")
            }
        }
    }

}
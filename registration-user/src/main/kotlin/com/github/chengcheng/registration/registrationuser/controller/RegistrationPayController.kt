package com.github.chengcheng.registration.registrationuser.controller

import com.github.chengcheng.registration.registrationcommon.ResponseInfo
import com.github.chengcheng.registration.registrationuser.entity.RegistrationPay
import com.github.chengcheng.registration.registrationuser.service.RegistrationPayService
import kotlinx.coroutines.reactor.mono
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/pay")
class RegistrationPayController {

    @Autowired
    lateinit var registrationPayService: RegistrationPayService


    /**
     * @api{POST} /pay 保存支付信息
     * @apiGroup 支付接口
     * @apiName  保存支付信息
     * @apiVersion 0.0.1
     * @apiDescription 用于保存支付信息
     *
     * @apiParam {String} infoId 考试信息id
     * @apiParam {String} userId 用户id
     * @apiParam {float} money 金钱
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "保存成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /pay
     */
    @PostMapping
    fun SavePayInfo(@RequestBody registrationPay: RegistrationPay): Mono<ResponseInfo<String>> {
        return mono {
           val flag=registrationPayService.transaction(registrationPay)
            when(flag){
                0-> return@mono ResponseInfo.sucess("缴费成功")
                1-> return@mono  ResponseInfo.failed("缴费金额不对,请重新缴费")
                2-> return@mono  ResponseInfo.failed("缴费时间错误")
                else ->{
                    return@mono ResponseInfo.failed("缴费失败,或已存在缴费")
                }
            }
        }
    }


    /**
     * @api{GET} /pay/user/:id 用于获取用户支付信息
     * @apiGroup 支付接口
     * @apiName 获取用户支付信息
     * @apiVersion 0.0.1
     * @apiDescription 用于获取用户支付信息
     *
     * @apiParam {number} id 用户id
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功",   "data": [
            {
            "id": 6,
            "infoId": 9,
            "userId": 1,
            "money": 80.00,
            "infoName": "数学",
            "type": "统考",
            "registrationTime": "2020-07-06 12:42:34",
            "examinationTime": "2020-07-24 12:42:34",
            "publicationTime": "2020-07-30 21:46:33",
            "transactiontime": "2020-07-21 17:32:33"
            }
            ]
        }
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
            {
            "data": "无效token令牌",
            "message": "暂未登录或token已经过期",
            "code": "401"
            }
     * @apiErrorExample {json} token失效:
            {
            "data": "token已失效",
            "message": "操作失败",
            "code": "500"
            }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /pay/user/:id
     */
    @GetMapping("/user/{id}")
    fun getUserRegistrationPay(@PathVariable("id") userId:Long):Mono<ResponseInfo<List<RegistrationPay>>>{
        return mono {
            ResponseInfo.sucess(registrationPayService.findRegistrationPayInfoByUserId(userId))
        }
    }

    /**
     * @api{GET} /exam/{id} 根据考试id获取支付信息
     * @apiGroup 支付接口
     * @apiName 根据考试id获取支付信息
     * @apiVersion 0.0.1
     * @apiDescription 根据考试id获取支付信息
     *
     * @apiParam {number} id 考试id
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功",    "data": [
                        {
                        "id": 6,
                        "infoId": 9,
                        "userId": 1,
                        "money": 80.00,
                        "infoName": "数学",
                        "type": "统考",
                        "registrationTime": "2020-07-06 12:42:34",
                        "examinationTime": "2020-07-24 12:42:34",
                        "publicationTime": "2020-07-30 21:46:33",
                        "transactiontime": "2020-07-21 17:32:33"
                        }
                        ]
                }
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /exam/:id
     */
    @PreAuthorize("hasRole('amin')")
    @GetMapping("/exam/{id}")
    fun getExamRegistrationPay(@PathVariable("id") infoId:Long):Mono<ResponseInfo<List<RegistrationPay>>>{
        return mono {
            ResponseInfo.sucess(registrationPayService.findRegistrationPayInfoByInfoId(infoId))
        }
    }
}
package com.github.chengcheng.registration.registrationuser.utils

import org.springframework.stereotype.Component
import java.lang.reflect.Field

@Component
class UpdateUtils {

    fun getNullPropertyNames(targetObj:Any,sourceObj:Any):Any{
        val field:Array<Field> = targetObj.javaClass.declaredFields
        for(index in 0..field.size-1){
            val name=upCaseKeyFirstChar(field[index].name)
            val get = targetObj.javaClass.getMethod("get"+name)
            val values = get.invoke(targetObj)
            if (values==null||values.toString().trim()==""){
                val fieldItem = get.invoke(sourceObj)
                field[index].setAccessible(true)
                field[index].set(targetObj,fieldItem)
            }
        }
        return targetObj
    }


    private fun upCaseKeyFirstChar(key: String?): String? {
        return if (Character.isUpperCase(key!![0])) {
            key
        } else {
            StringBuilder().append(Character.toUpperCase(key[0])).append(key.substring(1)).toString()
        }
    }
}
package com.github.chengcheng.registration.registrationuser.service

import com.github.chengcheng.registration.registrationuser.entity.RegistrationRole
import com.github.chengcheng.registration.registrationuser.repository.RegistrationRoleRepository
import com.github.chengcheng.registration.registrationuser.utils.UpdateUtils
import kotlinx.coroutines.flow.toList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class RegistrationRoleService {

    @Autowired
    private lateinit var registrationRoleRepository: RegistrationRoleRepository
    @Autowired
    private lateinit var updateUtils: UpdateUtils

    suspend fun addRole(role: RegistrationRole):Int{
        val count=registrationRoleRepository.findRoleByRoleValue(role.roleValue).toList().count()
        if (count>0) return 1
        role.creationTime= LocalDateTime.now()
        registrationRoleRepository.save(role)
        return 0

    }

    suspend fun getRoleList():List<RegistrationRole>{
       return registrationRoleRepository.findAll().toList()
    }

    suspend fun deleteRole(id:Long):Int{
        val user=registrationRoleRepository.findById(id)
        if (user==null){
            return 1
        }
        registrationRoleRepository.deleteById(id)
        return 0
    }

    suspend fun updateItem(role: RegistrationRole):Int{
        if (role.Id==null) return 1
        val roleInfo=registrationRoleRepository.findById(role.Id!!)
        if (roleInfo==null) return 2
        updateUtils.getNullPropertyNames(role,roleInfo)
        registrationRoleRepository.save(role)
        return 0
    }


}
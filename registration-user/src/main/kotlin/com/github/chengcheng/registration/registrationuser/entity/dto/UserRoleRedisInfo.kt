package com.github.chengcheng.registration.registrationuser.entity.dto

import com.github.chengcheng.registration.registrationuser.entity.RegistrationRole
import com.github.chengcheng.registration.registrationuser.entity.UserEntity
import java.io.Serializable

data class UserRoleRedisInfo(val userEntity: UserEntity,
                             val roles:List<RegistrationRole>
):Serializable
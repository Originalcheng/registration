package com.github.chengcheng.registration.registrationuser.repository

import com.github.chengcheng.registration.registrationuser.entity.RegistrationScore
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface RegistrationScoreRepository: CoroutineCrudRepository<RegistrationScore, Long> {

    @Query("delete from registration_score where info_id=:infoId")
    fun deleteByInfoId(infoId:Long)

}
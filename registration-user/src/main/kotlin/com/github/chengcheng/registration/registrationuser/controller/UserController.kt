package com.github.chengcheng.registration.registrationuser.controller

import com.github.chengcheng.registration.registrationcommon.ResponseInfo
import com.github.chengcheng.registration.registrationuser.entity.UserEntity
import com.github.chengcheng.registration.registrationuser.entity.dto.UpdataPassword
import com.github.chengcheng.registration.registrationuser.service.UserService
import kotlinx.coroutines.reactor.mono
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.security.Principal

@RequestMapping("/user")
@RestController
class UserController {

    @Autowired
    lateinit var userService: UserService


    /**
     * @api{POST} /user/register 用户注册
     * @apiGroup 用户接口
     * @apiName 用户注册
     * @apiVersion 0.0.1
     * @apiDescription 用于用户注册
     *
     * @apiParam {String} username 用户名
     * @apiParam {String} password 密码
     * @apiParam {String} nikename 真实姓名
     * @apiParam {String} personid 身份证号
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "注册成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":"账号已注册"}
     *
     * @apiSampleRequest /user/register
     */
    @PostMapping("/register")
    fun register(@RequestBody userEntity:UserEntity):Mono<ResponseInfo<String>>{
        if(!StringUtils.isEmpty(userEntity.nikename) && !StringUtils.isEmpty(userEntity.personid))
            return userService.register(userEntity)
        else  return Mono.just(ResponseInfo.failed("用户姓名和身份证号不能为空"))
    }

    /**
     * @api{GET} /user/info 获取当前用户信息
     * @apiGroup 用户接口
     * @apiName 获取用户信息
     * @apiVersion 0.0.1
     * @apiDescription 用于获取用户信息
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data":
     *     {
     *          "username": "",
     *           "password": "",
     *          "nikename": "",
     *          "personid": "",
     *         "id": ""
     *     }
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /user/info
     */
    @GetMapping("/info")
    fun getUserInfo(principal: Principal):Mono<ResponseInfo<UserEntity>>{
       return mono {
           val userinfo = userService.findUserByUserName(principal.name)
           userinfo.password=null
           return@mono ResponseInfo.sucess(userinfo)
        }
    }

    /**
     * @api{PUT} /user 更新用户信息
     * @apiGroup 用户接口
     * @apiName 用户更新
     * @apiVersion 0.0.1
     * @apiDescription  用于更新用户
     *
     * @apiParam {Number} id 用户id
     * @apiParam {String} [nikename] 真实姓名
     * @apiParam {String} [personid] 身份证号
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "修改成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /user
     */
    @PutMapping
    fun updataUserInfo(@RequestBody userEntity: UserEntity):Mono<ResponseInfo<String>>{
        return mono {
            if(userEntity.Id==null){
                return@mono ResponseInfo.failed("修改失败,当前用户Id不能为空")
            }
            val flag=userService.updateUserInfo(userEntity)
            when(flag){
                0->return@mono ResponseInfo.sucess("修改成功")
                1->return@mono ResponseInfo.failed("修改失败,没有当前用户")
                else->return@mono ResponseInfo.failed("修改失败")
            }
        }
    }


    /**
     * @api{GET}  /user 获取全部用户信息
     * @apiGroup 用户接口
     * @apiName 获取全部用户信息
     * @apiVersion 0.0.1
     * @apiDescription  用于获取全部用户信息
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": [
     *    {
            "username": "",
            "password": "",
            "nikename": "",
            "personid": "",
            "id": ""
    },
     *
     * ]}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /user
     */
    @PreAuthorize("hasRole('admin')")
    @GetMapping
    fun getAll():Mono<ResponseInfo<List<UserEntity>>>{
        return mono {
            ResponseInfo.sucess(userService.getUserList())
        }
    }

    /**
     * @api{PUT} /user/password/{userid}  更新用户密码
     * @apiGroup 用户接口
     * @apiName 更新用户密码
     * @apiVersion 0.0.1
     * @apiDescription 用于更新用户密码
     *
     * @apiParam {number} id 用户id
     * @apiParam {String} oldPassword 旧密码
     * @apiParam {String} newPassowrd 密码
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "修改成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest  /user/password/{userid}
     */
    @PutMapping("/password")
    fun updataPassword(@RequestBody updataPassword: UpdataPassword):Mono<ResponseInfo<String>>{
        return mono {
           val flag:Int = userService.updataUserPassword(updataPassword)
            when(flag){
                0-> ResponseInfo.sucess("修改成功")
                1-> ResponseInfo.failed("旧密码与原密码不匹配")
                2-> ResponseInfo.failed("用户id字段不能为空")
                3-> ResponseInfo.failed("新密码不能为空")
                4-> ResponseInfo.failed("旧密码不能为空")
                5-> ResponseInfo.failed("找不到该用户")
                else-> ResponseInfo.failed("更新失败")
            }
        }
    }


}
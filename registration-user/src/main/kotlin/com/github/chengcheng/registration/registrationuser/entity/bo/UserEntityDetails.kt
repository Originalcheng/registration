package com.github.chengcheng.registration.registrationuser.entity.bo

import com.github.chengcheng.registration.registrationuser.entity.RegistrationRole
import com.github.chengcheng.registration.registrationuser.entity.UserEntity
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class UserEntityDetails: UserDetails{

    private var userEntity: UserEntity
    private var roles:List<RegistrationRole>

    constructor(userEntity:UserEntity,roles:List<RegistrationRole>) {
        this.userEntity = userEntity;
        this.roles = roles;
    }

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return roles.filter{it->it.roleValue!=null}
                 .map { it->
                     SimpleGrantedAuthority(it.roleValue) }.toList()
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun getUsername(): String {
        return this.userEntity.username!!
    }

    override fun getPassword(): String {
        return this.userEntity.password!!
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }


    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun toString(): String {
        return "UserEntityDetails(userEntity=$userEntity, roles=$roles)"
    }


}
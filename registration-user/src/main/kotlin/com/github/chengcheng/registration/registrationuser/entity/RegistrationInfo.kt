package com.github.chengcheng.registration.registrationuser.entity

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.time.LocalDateTime


@Table("registration_info")
data class  RegistrationInfo(
        @Id var Id:Long?=null,

        @Column("name")
        var subjects:String?=null,

        @Column("type")
        var type:String?=null,

        @Column("money")
        var money: BigDecimal?=null,

        @Column("registration_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var registrationtime: LocalDateTime?=null,

        @Column("examination_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var examinationtime:LocalDateTime?=null,

        @Column("publication_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var publicationtime:LocalDateTime?=null
)

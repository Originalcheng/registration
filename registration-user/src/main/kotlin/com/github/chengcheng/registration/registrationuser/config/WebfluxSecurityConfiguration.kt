package com.github.chengcheng.registration.registrationuser.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authentication.AuthenticationWebFilter
import org.springframework.security.web.server.util.matcher.NegatedServerWebExchangeMatcher
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers
import reactor.core.publisher.Mono

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class WebfluxSecurityConfiguration {

    @Autowired
    lateinit var serverHttpBearerAuthenticationConverter: ServerHttpBearerAuthenticationConverter

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun springSecurityFilterChain(httpSecurity: ServerHttpSecurity): SecurityWebFilterChain {
        return httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .formLogin().disable()
                .logout().disable()
                .addFilterAt(bearerAuthenticationFilter(), SecurityWebFiltersOrder.AUTHENTICATION)
                .exceptionHandling()
                .accessDeniedHandler(MyAccessDeniedHandler())
                .and()
                .authorizeExchange()
                .pathMatchers("/login","/user/register").permitAll()
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                .anyExchange().authenticated()
                .and()
                .build()
    }

    private fun bearerAuthenticationFilter(): AuthenticationWebFilter {
        val bearerAuthenticationFilter = AuthenticationWebFilter(ReactiveAuthenticationManager { Mono.just(it) })
        bearerAuthenticationFilter.setRequiresAuthenticationMatcher(NegatedServerWebExchangeMatcher(ServerWebExchangeMatchers.pathMatchers("/login", "/user/register")))
        bearerAuthenticationFilter.setServerAuthenticationConverter(serverHttpBearerAuthenticationConverter)
        return bearerAuthenticationFilter
    }

}
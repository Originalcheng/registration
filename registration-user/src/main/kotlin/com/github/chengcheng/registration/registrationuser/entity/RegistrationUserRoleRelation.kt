package com.github.chengcheng.registration.registrationuser.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.io.Serializable

@Table("registration_user_role")
open class RegistrationUserRoleRelation : Serializable {
    @Id
    var Id:Long?=null

    @Column("user_id")
    var userId:Long?=null

    @Column("role_id")
    var roleId:Long?=null

    override fun toString(): String {
        return "RegistrationUserRoleRelation(Id=$Id, userId=$userId, roleId=$roleId)"
    }

}
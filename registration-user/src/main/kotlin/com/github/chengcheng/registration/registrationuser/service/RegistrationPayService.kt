package com.github.chengcheng.registration.registrationuser.service

import com.github.chengcheng.registration.registrationuser.entity.RegistrationPay
import com.github.chengcheng.registration.registrationuser.repository.RegistrationInfoRepository
import com.github.chengcheng.registration.registrationuser.repository.RegistrationPayRepository
import kotlinx.coroutines.flow.toList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset


@Service
class RegistrationPayService {

    @Autowired
    private lateinit var registrationPayRepository: RegistrationPayRepository
    @Autowired
    private lateinit var registrationInfoRepository: RegistrationInfoRepository

    suspend fun transaction(registrationPay: RegistrationPay):Int{
        val info=registrationInfoRepository.findById(registrationPay.infoId!!)
        val payInfo=registrationPayRepository.findUserAndInfoPayment(registrationPay.userId!!,registrationPay.infoId!!)
        if(info!=null&&payInfo.toList().isEmpty()){
            if(info.registrationtime!!.toInstant(ZoneOffset.of("+8")).toEpochMilli() < LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli()
                    && info.examinationtime!!.toInstant(ZoneOffset.of("+8")).toEpochMilli()>LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli()){
                if(registrationPay.money!!.setScale(2, BigDecimal.ROUND_HALF_UP)==info.money){
                    registrationPay.transactiontime= LocalDateTime.now()
                    registrationPay.infoName=info.subjects
                    registrationPay.publicationTime=info.publicationtime
                    registrationPay.examinationTime=info.examinationtime
                    registrationPay.registrationTime=info.registrationtime
                    registrationPay.type=info.type
                    registrationPayRepository.save(registrationPay)
                    return 0
                }else
                    return 1
            }
            return 2
        }
        return 3
    }

    suspend fun findRegistrationPayInfoByUserId(userId:Long):List<RegistrationPay>{
        return registrationPayRepository.findRegistrationPayByUserId(userId).toList()
    }

    suspend fun findRegistrationPayInfoByInfoId(infoId:Long):List<RegistrationPay>{
        return registrationPayRepository.findRegistrationPayByInfoId(infoId).toList()
    }
}
package com.github.chengcheng.registration.registrationuser.config

import com.github.chengcheng.registration.registrationuser.entity.RegistrationRole
import com.github.chengcheng.registration.registrationuser.entity.bo.UserEntityDetails
import com.github.chengcheng.registration.registrationuser.repository.UserRepository
import com.github.chengcheng.registration.registrationuser.service.RegistrationUserRoleRelationService
import com.github.chengcheng.registration.registrationuser.utils.JwtUtil
import kotlinx.coroutines.reactor.mono
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@Component
class ServerHttpBearerAuthenticationConverter : ServerAuthenticationConverter {
    @Autowired
    private lateinit var jwtUtil: JwtUtil
    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var registrationUserRoleRelationService: RegistrationUserRoleRelationService

    override fun convert(exchange: ServerWebExchange?): Mono<Authentication> {
        val request = exchange!!.request
        val Authorization=request.headers.getFirst("Authorization")!!.split(" ")
        val tokenStr=Authorization[1]
        val username=jwtUtil.getUserNameFromToken(tokenStr)
        return mono{
            val userinfo=userRepository.findUserByUsername(username)
            val roleList:List<RegistrationRole> = registrationUserRoleRelationService.getUserRoleRelation(userinfo!!.Id!!).map {
                it->it.roleValue="ROLE_${it.roleValue}"
                it
            }.toList()
            val userDetails= UserEntityDetails(userinfo,roleList)
            return@mono UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
        }
    }
}
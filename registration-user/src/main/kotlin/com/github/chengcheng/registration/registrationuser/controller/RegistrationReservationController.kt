package com.github.chengcheng.registration.registrationuser.controller

import com.github.chengcheng.registration.registrationcommon.ResponseInfo
import com.github.chengcheng.registration.registrationuser.entity.RegistrationReservation
import com.github.chengcheng.registration.registrationuser.entity.dto.ReservationInfo
import com.github.chengcheng.registration.registrationuser.service.RegistrationReservationService
import kotlinx.coroutines.reactor.mono
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/reserve")
class RegistrationReservationController {

    @Autowired
    lateinit var registrationReservationService: RegistrationReservationService

    /**
     * @api{POST} /reserve 添加预约信息
     * @apiGroup 预约接口
     * @apiName 添加预约信息
     * @apiVersion 0.0.1
     * @apiDescription  用于添加预约信息
     *
     * @apiParam {number} infoId 考试id
     * @apiParam {number} userId 用户id
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "预约成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":已经预约或预约失败}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /reserve
     */
    @PostMapping
    fun saveReserve(@RequestBody registrationReservation: RegistrationReservation):Mono<ResponseInfo<String>>{
        return mono {
         val flag=registrationReservationService.reserve(registrationReservation)
            when(flag){
                0->return@mono ResponseInfo.sucess("预约成功")
                1->return@mono ResponseInfo.failed("当前考试不存在,或已经存在预约信息")
                2->return@mono ResponseInfo.failed("预约时间错误")
                else->{
                  return@mono ResponseInfo.failed("已经预约或预约失败")
                }
            }
        }
    }
    /**
     * @api{Delete} /reserve/{id} 取消预约
     * @apiGroup 预约接口
     * @apiName 取消预约
     * @apiVersion 0.0.1
     * @apiDescription 用于取消预约
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": "取消成功"}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":"取消失败,或当前已不存在预约"}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /reserve/{id}
     */
    @DeleteMapping("/{id}")
    fun deleteReserve(@PathVariable("id") id:Long):Mono<ResponseInfo<String>>{
        return mono{
            val flag=registrationReservationService.cancelReserve(id)
            when(flag){
                1->    return@mono ResponseInfo.sucess("取消成功")
                else->  return@mono ResponseInfo.failed("取消失败,或当前已不存在预约")
            }
        }
    }

    /**
     * @api{POST} /reserve/all  获取全部预约信息
     * @apiGroup 预约接口
     * @apiName  获取全部预约信息
     * @apiVersion 0.0.1
     * @apiDescription 用于获取全部预约信息
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": [
         * {
                "infoId": "",
                "userId": "",
                "registrationtime": "",
                "subjects": "",
                "type": "",
                "money": "",
                "examinationtime": "",
                "publicationtime": "",
                "reservationtime": "",
                "id": ""
        }
     * ]}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /reserve/all
     */
    @PreAuthorize("hasRole('admin')")
    @GetMapping("/all")
    fun getAll():Mono<ResponseInfo<List<ReservationInfo>>>{
       return mono {
          ResponseInfo.sucess(registrationReservationService.getAllReserveInfo())
       }
    }


    /**
     * @api{POST} /reserve/all/{id} 获取当前用户所预约的考试信息
     * @apiGroup 预约接口
     * @apiName 获取当前用户所预约的考试信息
     * @apiVersion 0.0.1
     * @apiDescription 用于获取当前用户所预约的考试信息
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data": [
     * {
                "infoId": "",
                "userId": "",
                "registrationtime": "",
                "subjects": "",
                "type": "",
                "money": "",
                "examinationtime": "",
                "publicationtime": "",
                "reservationtime": "",
                "id": ""
                }
     * ]}
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":null}
     * @apiErrorExample {json} 未登录访问:
    {
    "data": "无效token令牌",
    "message": "暂未登录或token已经过期",
    "code": "401"
    }
     * @apiErrorExample {json} token失效:
    {
    "data": "token已失效",
    "message": "操作失败",
    "code": "500"
    }
     * @apiHeader {String}  Authorization 鉴权信息：为Bearer + "空格" +  {token}
     *
     * @apiSampleRequest /all/{id}
     */
    @GetMapping("/all/{id}")
    fun getAll(@PathVariable("id") id:Long):Mono<ResponseInfo<List<ReservationInfo>>>{
        return mono {
            ResponseInfo.sucess(registrationReservationService.getCurrentUserAllReserveInfo(id))
        }
    }
}
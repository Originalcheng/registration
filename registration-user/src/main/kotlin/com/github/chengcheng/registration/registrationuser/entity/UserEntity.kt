package com.github.chengcheng.registration.registrationuser.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.io.Serializable

@Table("registration_user")
data class UserEntity(@Id var Id:Long?=null,
                  @Column("user_name")var username:String?=null,
                  @Column("user_password") var password:String?=null,
                  @Column("nike_name") var nikename:String?=null,
                  @Column("user_person_id") var personid:String?=null): Serializable{

    override fun toString(): String {
        return "UserEntity(Id=$Id, username=$username, password=$password, nikename=$nikename, personid=$personid)"
    }
}

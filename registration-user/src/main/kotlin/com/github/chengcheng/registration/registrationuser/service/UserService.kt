package com.github.chengcheng.registration.registrationuser.service

import com.github.chengcheng.registration.registrationcommon.ResponseInfo
import com.github.chengcheng.registration.registrationuser.entity.UserEntity
import com.github.chengcheng.registration.registrationuser.entity.dto.UpdataPassword
import com.github.chengcheng.registration.registrationuser.repository.UserRepository
import com.github.chengcheng.registration.registrationuser.utils.CoroutineHandler
import com.github.chengcheng.registration.registrationuser.utils.UpdateUtils
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.toList
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserService {

    @Autowired
    private lateinit var passwordEncoder:PasswordEncoder
    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var coroutineHandler: CoroutineHandler
    @Autowired
    private lateinit var updateUtils: UpdateUtils

    fun register(userEntity: UserEntity): Mono<ResponseInfo<String>> = coroutineHandler.suspendHandler{
        if(!StringUtils.isEmpty(userEntity.username) && !StringUtils.isEmpty(userEntity.password)){
            if (userRepository.findUserByUsername(userEntity.username)== null){
                userEntity.password=passwordEncoder.encode(userEntity.password)
                userRepository.save(userEntity)
                return@suspendHandler  ResponseInfo.sucess("注册成功")
            }else return@suspendHandler ResponseInfo.failed("账号已注册,${userEntity.username}")
        }else return@suspendHandler ResponseInfo.failed("用户名或者账号不能为空")
    }

    suspend fun getUserList():List<UserEntity>{
        val  userList:Flow<UserEntity> = userRepository.findAll()
        val list:ArrayList<UserEntity> = ArrayList()
        userList.toList().forEach { it->it.password=null
            list.add(it)
        }
        return list
    }

    suspend fun updateUserInfo(userEntity: UserEntity):Int{
        val user=userRepository.findById(userEntity.Id!!)
        if(user==null){
            return 1
        }
        userEntity.username=null
        userEntity.password=null
        updateUtils.getNullPropertyNames(userEntity,user)
        userRepository.save(userEntity)
        return 0
    }

    suspend fun findUserByUserName(username:String):UserEntity{
        return userRepository.findUserByUsername(username)!!
    }

    suspend fun updataUserPassword(updataPassword: UpdataPassword):Int{
        if (updataPassword.id==null){
            return 2
        }
        if (updataPassword.newPassword==null){
            return 3
        }
        if (updataPassword.oldPassword==null){
            return 4
        }
        val userinfo=userRepository.findById(updataPassword.id)
        if (userinfo==null){
            return 5
        }
        if (passwordEncoder.matches(updataPassword.oldPassword,userinfo!!.password)){
            userinfo.password=passwordEncoder.encode(updataPassword.newPassword)
            userRepository.save(userinfo)
            return 0
        }
        return 1
    }


}
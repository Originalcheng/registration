package com.github.chengcheng.registration.registrationuser.entity.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.github.chengcheng.registration.registrationuser.entity.RegistrationReservation
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.time.LocalDateTime

class ReservationInfo: RegistrationReservation() {

        var subjects:String?=null

        var type:String?=null

        var money: BigDecimal?=null

        @Column("examination_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var examinationtime: LocalDateTime?=null

        @Column("publication_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var publicationtime: LocalDateTime?=null

        @Column("reservation_time")
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var reservationtime:LocalDateTime?=null

        override fun toString(): String {
                return "ReservationInfo(Id=$Id,userId=$userId,infoId=$infoId,subjects=$subjects, type=$type, money=$money,registrationtime=$registrationtime, examinationtime=$examinationtime, publicationtime=$publicationtime, reservationtime=$reservationtime)"
        }

}
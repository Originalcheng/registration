package com.github.chengcheng.registration.registrationuser.service

import com.github.chengcheng.registration.registrationuser.entity.RegistrationInfo
import com.github.chengcheng.registration.registrationuser.repository.RegistrationInfoRepository
import com.github.chengcheng.registration.registrationuser.repository.RegistrationPayRepository
import com.github.chengcheng.registration.registrationuser.repository.RegistrationReservationRepository
import com.github.chengcheng.registration.registrationuser.utils.CoroutineHandler
import com.github.chengcheng.registration.registrationuser.utils.UpdateUtils
import kotlinx.coroutines.flow.toList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class RegistrationInfoService {

    @Autowired
    private lateinit var registrationInfoRepository: RegistrationInfoRepository
    @Autowired
    private lateinit var coroutineHandler: CoroutineHandler
    @Autowired
    private lateinit var registrationReservationRepository: RegistrationReservationRepository
    @Autowired
    private lateinit var updateUtils: UpdateUtils

    fun saveInfo(info: RegistrationInfo):Mono<RegistrationInfo>
            = coroutineHandler.suspendHandler{
         return@suspendHandler registrationInfoRepository.save(info)
    }

    suspend fun getAll():List<RegistrationInfo> {
       return registrationInfoRepository.findAll().toList()
    }


    suspend fun deleteItemById(id:Long):Int{
       val info:RegistrationInfo? = registrationInfoRepository.findById(id)
        if (info==null){
            return 1
        }
        //删除考试相对应的预约信息
        registrationReservationRepository.deleteByInfoId(info.Id!!)
        registrationInfoRepository.deleteById(id)
        return 0
    }


    suspend fun getInfoByID(id:Long):RegistrationInfo?{
       return registrationInfoRepository.findById(id)
    }

    suspend fun updateInfo(registrationInfo:RegistrationInfo):Int{
        if (registrationInfo.Id==null) return 1
        val info=registrationInfoRepository.findById(registrationInfo.Id!!)
        if (info==null) return 2
        registrationInfoRepository.save(updateUtils.getNullPropertyNames(registrationInfo,info)
                as RegistrationInfo)
        return 0
    }
}
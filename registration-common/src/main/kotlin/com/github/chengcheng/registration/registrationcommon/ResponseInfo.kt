package com.github.chengcheng.registration.registrationcommon

import java.io.Serializable

class ResponseInfo<T>(val code:Int?=null,
                      val message:String?=null) : Serializable {

    var data:T?=null

    constructor(code: Int, message: String, data: T) : this(code, message) {
        this.data = data
    }

    companion object {
        @JvmStatic
        fun <T> sucess(data: T): ResponseInfo<T> {
            return ResponseInfo(ResponseCode.SUCCESS.code, ResponseCode.SUCCESS.message, data);
        }

        @JvmStatic
        fun <T> failed(data: T): ResponseInfo<T> {
            return ResponseInfo(ResponseCode.FAILED.code, ResponseCode.FAILED.message, data);
        }

        @JvmStatic
        fun <T> unauthorized(data: T): ResponseInfo<T> {
            return ResponseInfo(ResponseCode.UNAUTHORIZED.code, ResponseCode.UNAUTHORIZED.message, data);
        }
    }

    override fun toString(): String {
        return "{" +
                "\"data\":\"$data\"," +
                "\"message\":\"$message\"," +
                "\"code\":\"$code\"" +
                "}"
    }
}
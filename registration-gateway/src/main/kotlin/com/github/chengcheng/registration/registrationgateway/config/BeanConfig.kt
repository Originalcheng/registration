package com.github.chengcheng.registration.registrationgateway.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.PredicateSpec
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.RequestPredicates
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions


@Configuration
class BeanConfig{

    @Autowired
    lateinit var authHandler:AuthHandler

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
   fun router():RouterFunction<*>{
        return RouterFunctions
                .route(RequestPredicates.POST("/login")
                        .and(RequestPredicates.accept(MediaType.APPLICATION_JSON)), HandlerFunction { authHandler.login(it) })
    }
}
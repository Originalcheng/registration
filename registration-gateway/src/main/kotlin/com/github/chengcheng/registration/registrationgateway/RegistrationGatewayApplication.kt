package com.github.chengcheng.registration.registrationgateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RegistrationGatewayApplication

fun main(args: Array<String>) {
    runApplication<RegistrationGatewayApplication>(*args)
}

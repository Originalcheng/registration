package com.github.chengcheng.registration.registrationgateway.config

import com.github.chengcheng.registration.registrationcommon.ResponseInfo
import com.github.chengcheng.registration.registrationgateway.bean.LoginParam
import com.github.chengcheng.registration.registrationgateway.repository.UserRepository
import com.github.chengcheng.registration.registrationgateway.service.UserDetailsService
import com.github.chengcheng.registration.registrationgateway.utils.JwtUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.web.cors.reactive.CorsUtils.isCorsRequest
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import java.nio.charset.StandardCharsets


@Configuration
class CustomFilterConfiguration:WebFilter {

    @Autowired
    lateinit var userDetailsService: UserDetailsService
    @Autowired
    lateinit var jwtUtil: JwtUtil

    private val ALLOWED_HEADERS = "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN,token,username,client"
    private val ALLOWED_METHODS = "*"
    private val ALLOWED_ORIGIN = "*"
    private val ALLOWED_Expose = "*"
    private val TOKEN_HEADER= "Bearer"
    private val MAX_AGE = "18000L"



    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val request = exchange.request
        if (isCorsRequest(request)) {
            val response: org.springframework.http.server.reactive.ServerHttpResponse = exchange.response
            val headers: HttpHeaders = response.headers
            headers.add("Access-Control-Allow-Origin", ALLOWED_ORIGIN)
            headers.add("Access-Control-Allow-Methods", ALLOWED_METHODS)
            headers.add("Access-Control-Allow-Headers", ALLOWED_HEADERS)
            headers.add("Access-Control-Expose-Headers", ALLOWED_Expose)
            headers.add("Access-Control-Allow-Credentials", "true")
            if (request.getMethod() == HttpMethod.OPTIONS) {
                response.setStatusCode(HttpStatus.OK);
                return Mono.empty();
            }
        }
        if(request.path.toString()!="/login" && request.path.toString()!="/user/register" ){
            //TODO 加入跳过的接口错误和错误处理
            if (request.headers.getFirst("Authorization")==null ||
                    request.headers.getFirst("Authorization")!!.split(" ").size<2){
                return authError(exchange, Mono.just(ResponseInfo.unauthorized("无效token令牌")))
            }
            val Authorization=request.headers.getFirst("Authorization")!!.split(" ")
            val tokenHeader=Authorization[0]
            val tokenStr=Authorization[1]
            if(tokenHeader!=TOKEN_HEADER){
                return authError(exchange, Mono.just(ResponseInfo.unauthorized("无效token令牌")))
            }
            val username=jwtUtil.getUserNameFromToken(tokenStr)
            if (username==null){
                return authError(exchange, Mono.just(ResponseInfo.unauthorized("无效token令牌")))
            }
           return userDetailsService.loadUserByUserName(username)
                    .flatMap {
                        if (!jwtUtil.validateToken(tokenStr, LoginParam(it.username,it.password))){
                            return@flatMap authError(exchange, Mono.just(ResponseInfo.failed("token已失效")))
                        }else  return@flatMap chain.filter(exchange)
                    } .switchIfEmpty(authError(exchange,Mono.just(ResponseInfo.failed("登陆失败"))))
        }
        return chain.filter(exchange)
    }


    private fun authError(swe: ServerWebExchange,res:Mono<ResponseInfo<String>>): Mono<Void> {
        val resp = swe.response
        resp.statusCode = HttpStatus.UNAUTHORIZED
        resp.headers.add("Content-Type", "application/json;charset=UTF-8")
        val string=res.flatMap {
            val buffer = resp.bufferFactory().wrap(it.toString().toByteArray(StandardCharsets.UTF_8))
            return@flatMap Mono.just(buffer)
        }
        return resp.writeWith(string)
    }


}
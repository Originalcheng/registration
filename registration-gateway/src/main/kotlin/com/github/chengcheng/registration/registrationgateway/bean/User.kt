package com.github.chengcheng.registration.registrationgateway.bean

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.io.Serializable

@Table("registration_user")
open  class User:Serializable{
    @Id var id:Long?=null
    @Column("user_name")  var username:String?=null
    @Column("user_password") var password:String?=null
    @Column("nike_name") var nikName:String?=null
    @Column("user_person_id") var userPersonId:String?=null
}
package com.github.chengcheng.registration.registrationgateway.bean

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDateTime

@Table("registration_role")
class RegistrationRole {
    @Id
    var Id:Long?=null

    @Column("role_name")
    var roleName:String?=null

    @Column("role_value")
    var roleValue:String?=null

    @Column("creation_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    var creationTime:LocalDateTime?=null

    override fun toString(): String {
        return "RegistrationRole{'Id'=$Id, roleName=$roleName, 'roleValue'=$roleValue, 'creationTime'=$creationTime}"
    }

}
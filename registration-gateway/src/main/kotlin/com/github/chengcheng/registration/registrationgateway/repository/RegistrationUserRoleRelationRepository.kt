package com.github.chengcheng.registration.registrationgateway.repository

import com.github.chengcheng.registration.registrationgateway.bean.RegistrationRole
import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query

import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface RegistrationUserRoleRelationRepository : CoroutineCrudRepository<RegistrationRole, Long> {

    @Query("SELECT ro.* FROM `registration_user_role` as ur \n" +
            "RIGHT JOIN `registration_role` as ro ON ro.id=ur.role_id \n" +
            "WHERE ur.user_id=:userId")
    fun getUserCorrespondRoleList(userId:Long): Flow<RegistrationRole>
}
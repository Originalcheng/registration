package com.github.chengcheng.registration.registrationgateway.config

import com.github.chengcheng.registration.registrationcommon.ResponseInfo
import com.github.chengcheng.registration.registrationgateway.bean.LoginParam
import com.github.chengcheng.registration.registrationgateway.repository.UserRepository
import com.github.chengcheng.registration.registrationgateway.service.UserDetailsService
import com.github.chengcheng.registration.registrationgateway.utils.JwtUtil
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

@Configuration
class AuthHandler {

    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var passwordEncoder: PasswordEncoder
    @Autowired
    lateinit var jwtUtil: JwtUtil
    @Autowired
    lateinit var userDetailsService: UserDetailsService
    /**
     * @api{POST} /login  用户登陆接口
     * @apiGroup 用户接口
     * @apiName 用户登陆
     * @apiVersion 0.0.1
     * @apiDescription  用于用户登陆
     *
     * @apiParam {String}  username  用户名
     * @apiParam {String}  password  密码
     *
     * @apiSuccess {Number} code    状态码，200：有数据/成功，其他：没数据/失败
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} data    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code": 200,"message": "操作成功","data":  {"userId": 1,
     * "nikename": "",
     * "tokenStr": ""
     * }
    }
     *
     * @apiErrorExample {json} Error-Response:
     * {"status":500,"msg":"操作失败","data":"该账号尚未注册"}
     *
     * @apiSampleRequest /login
     */
    fun login(request: ServerRequest):Mono<ServerResponse>{
        val body:Mono<LoginParam> =request.bodyToMono(LoginParam::class.java)
        return body.flatMap {
            param->
            if(!StringUtils.isEmpty(param.username) && !StringUtils.isEmpty(param.password)){
                return@flatMap  userDetailsService.loadUserByUserName(param.username!!)
                        .flatMap {
                           val resp:ResponseInfo<Any> = if (passwordEncoder.matches(param.password,it.password)){
                               val token: String = jwtUtil.generateToken(LoginParam(it.username, it.password))
                               it.password=null
                               it.tokenStr=token
                               ResponseInfo.sucess(it)
                           }else{
                               ResponseInfo.failed("密码错误")
                           }
                            ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                                    .body(BodyInserters.fromValue(resp))
                        }.switchIfEmpty(ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                                .body(BodyInserters.fromValue(ResponseInfo.failed("该账号尚未注册,${param.username}"))))
            }else return@flatMap ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(
                BodyInserters.fromValue(ResponseInfo.failed("账号密码不能为空"))
            )
        }.switchIfEmpty(ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(
                        BodyInserters.fromValue(ResponseInfo.failed("账号密码不能为空"))))
    }
}
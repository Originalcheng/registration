package com.github.chengcheng.registration.registrationgateway.service

import com.github.chengcheng.registration.registrationgateway.bean.RegistrationRole
import com.github.chengcheng.registration.registrationgateway.bean.UserInfoParams
import com.github.chengcheng.registration.registrationgateway.repository.RegistrationUserRoleRelationRepository
import com.github.chengcheng.registration.registrationgateway.repository.UserRepository
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactor.mono
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserDetailsService {

    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var registrationUserRoleRelationRepository: RegistrationUserRoleRelationRepository

    fun loadUserByUserName(userName: String):Mono<UserInfoParams>{
        return mono {
            val userinfo = userRepository.findUserByUserName(userName)
            if (userinfo==null){
                return@mono null
            }
            val roles:List<RegistrationRole> = registrationUserRoleRelationRepository.getUserCorrespondRoleList(userinfo.id!!).toList()
            userinfo.roles = roles
            return@mono userinfo
        }
    }
}
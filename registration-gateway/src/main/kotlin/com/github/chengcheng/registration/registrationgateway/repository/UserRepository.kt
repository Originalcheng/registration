package com.github.chengcheng.registration.registrationgateway.repository

import com.github.chengcheng.registration.registrationgateway.bean.User
import com.github.chengcheng.registration.registrationgateway.bean.UserInfoParams
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface UserRepository: ReactiveCrudRepository<User,Long> {

    @Query("select * from registration_user where user_name=:username limit 1")
    suspend fun findUserByUserName(username:String?): UserInfoParams?

}
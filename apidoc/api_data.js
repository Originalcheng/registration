define({ "api": [
  {
    "type": "POST",
    "url": "/pay",
    "title": "保存支付信息",
    "group": "支付接口",
    "name": "保存支付信息",
    "version": "0.0.1",
    "description": "<p>用于保存支付信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "infoId",
            "description": "<p>考试信息id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "float",
            "optional": false,
            "field": "money",
            "description": "<p>金钱</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"保存成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/pay"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationPayController.kt",
    "groupTitle": "支付接口"
  },
  {
    "type": "GET",
    "url": "/exam/{id}",
    "title": "根据考试id获取支付信息",
    "group": "支付接口",
    "name": "根据考试id获取支付信息",
    "version": "0.0.1",
    "description": "<p>根据考试id获取支付信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>考试id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",    \"data\": [\n                        {\n                        \"id\": 6,\n                        \"infoId\": 9,\n                        \"userId\": 1,\n                        \"money\": 80.00,\n                        \"infoName\": \"数学\",\n                        \"type\": \"统考\",\n                        \"registrationTime\": \"2020-07-06 12:42:34\",\n                        \"examinationTime\": \"2020-07-24 12:42:34\",\n                        \"publicationTime\": \"2020-07-30 21:46:33\",\n                        \"transactiontime\": \"2020-07-21 17:32:33\"\n                        }\n                        ]\n                }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exam/:id"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationPayController.kt",
    "groupTitle": "支付接口"
  },
  {
    "type": "GET",
    "url": "/pay/user/:id",
    "title": "用于获取用户支付信息",
    "group": "支付接口",
    "name": "获取用户支付信息",
    "version": "0.0.1",
    "description": "<p>用于获取用户支付信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",   \"data\": [\n            {\n            \"id\": 6,\n            \"infoId\": 9,\n            \"userId\": 1,\n            \"money\": 80.00,\n            \"infoName\": \"数学\",\n            \"type\": \"统考\",\n            \"registrationTime\": \"2020-07-06 12:42:34\",\n            \"examinationTime\": \"2020-07-24 12:42:34\",\n            \"publicationTime\": \"2020-07-30 21:46:33\",\n            \"transactiontime\": \"2020-07-21 17:32:33\"\n            }\n            ]\n        }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/pay/user/:id"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationPayController.kt",
    "groupTitle": "支付接口"
  },
  {
    "type": "PUT",
    "url": "/user/password/{userid}",
    "title": "更新用户密码",
    "group": "用户接口",
    "name": "更新用户密码",
    "version": "0.0.1",
    "description": "<p>用于更新用户密码</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "oldPassword",
            "description": "<p>旧密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "newPassowrd",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"修改成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/user/password/{userid}"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/UserController.kt",
    "groupTitle": "用户接口"
  },
  {
    "type": "PUT",
    "url": "/user",
    "title": "更新用户信息",
    "group": "用户接口",
    "name": "用户更新",
    "version": "0.0.1",
    "description": "<p>用于更新用户</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "nikename",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "personid",
            "description": "<p>身份证号</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"修改成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/user"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/UserController.kt",
    "groupTitle": "用户接口"
  },
  {
    "type": "POST",
    "url": "/user/register",
    "title": "用户注册",
    "group": "用户接口",
    "name": "用户注册",
    "version": "0.0.1",
    "description": "<p>用于用户注册</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nikename",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "personid",
            "description": "<p>身份证号</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"注册成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":\"账号已注册\"}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/user/register"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/UserController.kt",
    "groupTitle": "用户接口"
  },
  {
    "type": "POST",
    "url": "/login",
    "title": "用户登陆接口",
    "group": "用户接口",
    "name": "用户登陆",
    "version": "0.0.1",
    "description": "<p>用于用户登陆</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\":  {\"userId\": 1,\n\"nikename\": \"\",\n\"tokenStr\": \"\"\n}\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":\"该账号尚未注册\"}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/login"
      }
    ],
    "filename": "registration-gateway/src/main/kotlin/com/github/chengcheng/registration/registrationgateway/config/AuthHandler.kt",
    "groupTitle": "用户接口"
  },
  {
    "type": "GET",
    "url": "/user",
    "title": "获取全部用户信息",
    "group": "用户接口",
    "name": "获取全部用户信息",
    "version": "0.0.1",
    "description": "<p>用于获取全部用户信息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": [\n   {\n            \"username\": \"\",\n            \"password\": \"\",\n            \"nikename\": \"\",\n            \"personid\": \"\",\n            \"id\": \"\"\n    },\n\n]}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/user"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/UserController.kt",
    "groupTitle": "用户接口"
  },
  {
    "type": "GET",
    "url": "/user/info",
    "title": "获取当前用户信息",
    "group": "用户接口",
    "name": "获取用户信息",
    "version": "0.0.1",
    "description": "<p>用于获取用户信息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\":\n    {\n         \"username\": \"\",\n          \"password\": \"\",\n         \"nikename\": \"\",\n         \"personid\": \"\",\n        \"id\": \"\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/user/info"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/UserController.kt",
    "groupTitle": "用户接口"
  },
  {
    "type": "POST",
    "url": "/exam",
    "title": "保存考试信息",
    "group": "考试接口",
    "name": "保存考试信息",
    "version": "0.0.1",
    "description": "<p>保存考试信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "subjects",
            "description": "<p>科目</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>类型</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "money",
            "description": "<p>报名费用</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "registrationtime",
            "description": "<p>报名时间</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "examinationtime",
            "description": "<p>考试时间</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "publicationtime",
            "description": "<p>公布时间</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"保存成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exam"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationInfoController.kt",
    "groupTitle": "考试接口"
  },
  {
    "type": "Delete",
    "url": "/exam//{id}",
    "title": "删除当前考试信息",
    "group": "考试接口",
    "name": "删除当前考试信息",
    "version": "0.0.1",
    "description": "<p>删除当前考试信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "exam_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",  \"data\": \"删除成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":\"删除失败,或当前考试不存在\"}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exam/{id}"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationInfoController.kt",
    "groupTitle": "考试接口"
  },
  {
    "type": "PUT",
    "url": "/exam",
    "title": "更新考试信息",
    "group": "考试接口",
    "name": "更新考试信息",
    "version": "0.0.1",
    "description": "<p>用于更新考试信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>科目</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "subjects",
            "description": "<p>科目</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>类型</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "money",
            "description": "<p>报名费用</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "registrationtime",
            "description": "<p>报名时间</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "examinationtime",
            "description": "<p>考试时间</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "publicationtime",
            "description": "<p>公布时间</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"保存成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exam"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationInfoController.kt",
    "groupTitle": "考试接口"
  },
  {
    "type": "GET",
    "url": "/exam/all",
    "title": "获取所有考试信息",
    "group": "考试接口",
    "name": "获取全部考试信息",
    "version": "0.0.1",
    "description": "<p>获取全部考试信息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": [\n   {\n            \"subjects\": \"语文\",\n            \"type\": \"统考\",\n            \"moeny\": 40.00,\n            \"registrationtime\": \"2020-07-06 12:42:34\",\n            \"examinationtime\": \"2020-07-15 12:42:34\",\n            \"publicationtime\": \"2020-07-31 21:46:33\",\n            \"id\": 3\n        },\n        {\n        \"subjects\": \"数学\",\n        \"type\": \"统考\",\n        \"moeny\": 80.00,\n        \"registrationtime\": \"2020-07-06 12:42:34\",\n        \"examinationtime\": \"2020-07-24 12:42:34\",\n        \"publicationtime\": \"2020-07-30 21:46:33\",\n        \"id\": 4\n        }\n]}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exam/all"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationInfoController.kt",
    "groupTitle": "考试接口"
  },
  {
    "type": "GET",
    "url": "/exam/{id}",
    "title": "获取考试信息",
    "group": "考试接口",
    "name": "获取考试信息",
    "version": "0.0.1",
    "description": "<p>用于获取考试信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>考试信息id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": {\n \"subjects\": \"数学\",\n \"type\": \"统考\",\n \"money\": 80.00,\n \"registrationtime\": \"2020-07-06 12:42:34\",\n \"examinationtime\": \"2020-07-24 12:42:34\",\n \"publicationtime\": \"2020-07-30 21:46:33\",\n \"id\": 5\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exam/{id}"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationInfoController.kt",
    "groupTitle": "考试接口"
  },
  {
    "type": "Delete",
    "url": "/role/{id}",
    "title": "删除角色",
    "group": "角色接口",
    "name": "删除角色",
    "version": "0.0.1",
    "description": "<p>用于删除角色</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>角色id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"删除成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":\"删除失败,不存在当前用户\"}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/role/{id}"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationRoleController.kt",
    "groupTitle": "角色接口"
  },
  {
    "type": "PUT",
    "url": "/role",
    "title": "更新角色信息",
    "group": "角色接口",
    "name": "更新角色信息",
    "version": "0.0.1",
    "description": "<p>用于更新角色信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "Id",
            "description": "<p>角色id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "roleName",
            "description": "<p>角色名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "roleValue",
            "description": "<p>角色值</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"修改成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":\"修改失败,或角色id不能为空\"}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/role"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationRoleController.kt",
    "groupTitle": "角色接口"
  },
  {
    "type": "POST",
    "url": "/role/relation",
    "title": "添加用户角色关系",
    "group": "角色接口",
    "name": "添加用户角色关系",
    "version": "0.0.1",
    "description": "<p>用于添加用户角色关系</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "roleId",
            "description": "<p>角色id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"保存成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":\"保存失败,或当前已存在相同的关系\"}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/role/relation"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationRoleController.kt",
    "groupTitle": "角色接口"
  },
  {
    "type": "POST",
    "url": "/role",
    "title": "添加角色",
    "group": "角色接口",
    "name": "添加角色",
    "version": "0.0.1",
    "description": "<p>用于添加角色</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "roleName",
            "description": "<p>角色名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "roleValue",
            "description": "<p>角色值</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"添加成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/role"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationRoleController.kt",
    "groupTitle": "角色接口"
  },
  {
    "type": "GET",
    "url": "/role/all",
    "title": "获取全部角色",
    "group": "角色接口",
    "name": "获取全部角色",
    "version": "0.0.1",
    "description": "<p>用于获取全部角色</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": [\n   {\n            \"roleName\": \"\",\n            \"roleValue\": \"\",\n            \"creationTime\": \"\",\n            \"id\": \"\"\n            }\n        ]\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/role/all"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationRoleController.kt",
    "groupTitle": "角色接口"
  },
  {
    "type": "POST",
    "url": "/role/relation/{id}",
    "title": "当前用户角色对应关系",
    "group": "角色接口",
    "name": "获取当前用户对应的角色",
    "version": "0.0.1",
    "description": "<p>用于获取当前用户对应的角色</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>角色id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": [\n                {\n                \"userId\": \"\",\n                \"roleId\": \"\",\n                \"roleName\": \"\",\n                \"roleValue\": \"\",\n                \"createtime\": \"\",\n                \"id\": \"\"\n                }\n        ]\n    }",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/role/relation/{id}"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationRoleController.kt",
    "groupTitle": "角色接口"
  },
  {
    "type": "Delete",
    "url": "/reserve/{id}",
    "title": "取消预约",
    "group": "预约接口",
    "name": "取消预约",
    "version": "0.0.1",
    "description": "<p>用于取消预约</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"取消成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":\"取消失败,或当前已不存在预约\"}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/reserve/{id}"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationReservationController.kt",
    "groupTitle": "预约接口"
  },
  {
    "type": "POST",
    "url": "/reserve",
    "title": "添加预约信息",
    "group": "预约接口",
    "name": "添加预约信息",
    "version": "0.0.1",
    "description": "<p>用于添加预约信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "infoId",
            "description": "<p>考试id</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "userId",
            "description": "<p>用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": \"预约成功\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":已经预约或预约失败}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/reserve"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationReservationController.kt",
    "groupTitle": "预约接口"
  },
  {
    "type": "POST",
    "url": "/reserve/all",
    "title": "获取全部预约信息",
    "group": "预约接口",
    "name": "获取全部预约信息",
    "version": "0.0.1",
    "description": "<p>用于获取全部预约信息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": [\n{\n                \"infoId\": \"\",\n                \"userId\": \"\",\n                \"registrationtime\": \"\",\n                \"subjects\": \"\",\n                \"type\": \"\",\n                \"money\": \"\",\n                \"examinationtime\": \"\",\n                \"publicationtime\": \"\",\n                \"reservationtime\": \"\",\n                \"id\": \"\"\n        }\n]}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/reserve/all"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationReservationController.kt",
    "groupTitle": "预约接口"
  },
  {
    "type": "POST",
    "url": "/reserve/all/{id}",
    "title": "获取当前用户所预约的考试信息",
    "group": "预约接口",
    "name": "获取当前用户所预约的考试信息",
    "version": "0.0.1",
    "description": "<p>用于获取当前用户所预约的考试信息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码，200：有数据/成功，其他：没数据/失败</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>提示信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"code\": 200,\"message\": \"操作成功\",\"data\": [\n{\n                \"infoId\": \"\",\n                \"userId\": \"\",\n                \"registrationtime\": \"\",\n                \"subjects\": \"\",\n                \"type\": \"\",\n                \"money\": \"\",\n                \"examinationtime\": \"\",\n                \"publicationtime\": \"\",\n                \"reservationtime\": \"\",\n                \"id\": \"\"\n                }\n]}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":500,\"msg\":\"操作失败\",\"data\":null}",
          "type": "json"
        },
        {
          "title": "未登录访问:",
          "content": "{\n\"data\": \"无效token令牌\",\n\"message\": \"暂未登录或token已经过期\",\n\"code\": \"401\"\n}",
          "type": "json"
        },
        {
          "title": "token失效:",
          "content": "{\n\"data\": \"token已失效\",\n\"message\": \"操作失败\",\n\"code\": \"500\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>鉴权信息：为Bearer + &quot;空格&quot; +  {token}</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/all/{id}"
      }
    ],
    "filename": "registration-user/src/main/kotlin/com/github/chengcheng/registration/registrationuser/controller/RegistrationReservationController.kt",
    "groupTitle": "预约接口"
  }
] });
